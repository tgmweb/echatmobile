//var remote = require('electron').remote;
var socketID;
var originalDocTitle = document.title;
var tabFlashTimeout;
var idleTime = 0;
var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
var chatChannels = [];
var presenceChannel;
var currentStatus;
var domain = localStorage.getItem("eChatdomain");
var site_id;
var currentUser;
//var AutoLaunch = require('auto-launch');
/*appLauncher = new AutoLaunch({
  name: 'eBiz Chat'
});*/
$(document).off("click","#autoStart").on("click","#autoStart",function(){

  if ($(this).prop("checked")) {
    appLauncher.enable();
  } else {
    appLauncher.disable();
  }
})
$(document).off("click","#openSettings").on("click","#openSettings",function(){
  $("#settings").toggleClass("hidden");
});
// Get user info
$.ajax({
  url: "http://" + domain + "/eChat/teams/getUserInfo",
  method: "POST",
  async: false,
  success: function (obj) {
    //var obj = $.parseJSON(data);
    $('#userEmail').val(obj.EMAIL);
    $('#userName').val(obj.USERNAME);
    $('#userID').val(obj.CONTACTID);
    site_id = obj.SITEID;
    currentUser = obj.CONTACTID;
    //console.log(obj.contactID);
  }
});

//WebSocket.__swfLocation = "./WebSocketMain.swf";
$.flashTitle = function (newMsg, howManyTimes) {
  function step() {
    document.title = (document.title == originalDocTitle) ? newMsg : originalDocTitle;
    if (--howManyTimes > 0) {
      tabFlashTimeout = setTimeout(step, 1000);
    }
    ;
  };

  howManyTimes = parseInt(howManyTimes);

  if (isNaN(howManyTimes)) {
    howManyTimes = 5;
  }
  ;

  $.cancelFlashTitle(tabFlashTimeout);
  step();
};

$.cancelFlashTitle = function () {
  clearTimeout(tabFlashTimeout);
  document.title = originalDocTitle;
};

$.fn.alterClass = function (removals, additions) {

  var self = this;

  if (removals.indexOf('*') === -1) {
    // Use native jQuery methods if there is no wildcard matching
    self.removeClass(removals);
    return !additions ? self : self.addClass(additions);
  }

  var patt = new RegExp('\\s' +
      removals.replace(/\*/g, '[A-Za-z0-9-_]+').split(' ').join('\\s|\\s') +
      '\\s', 'g');

  self.each(function (i, it) {
    var cn = ' ' + it.className + ' ';
    while (patt.test(cn)) {
      cn = cn.replace(patt, ' ');
    }
    it.className = $.trim(cn);
  });

  return !additions ? self : self.addClass(additions);
};

Storage.prototype.setObject = function (key, value) {
  this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function (key) {
  var value = this.getItem(key);
  return value && JSON.parse(value);
}
localStorage.setObject("chatWindows", {});
currentStatus = localStorage.getItem("chatStatus");
// console.log(currentStatus);
if (currentStatus == null) {
  currentStatus = "online";
}

var pusherAppKey = "8f249cb26ac6821aef06";
var pusher = new Pusher(pusherAppKey, {
  authEndpoint: 'http://' + domain + '/eChat/auth',
  auth: {
    params: {
      currentStatus: currentStatus
    }
  }
});


//console.log(currentStatus);
function changeStatus(cStatus) {
  if (cStatus == "dnd") {
    localStorage.setObject("chatWindows", {});
    $(".chatWindow").hide();
    $.get("http://" + domain + "/eChat/status/setStatus?status=dnd");
    localStorage.setItem("chatStatus", "dnd");
    currentStatus = "dnd";
  } else if (cStatus == "away") {
    $.get("http://" + domain + "/eChat/status/setStatus?status=away");
    localStorage.setItem("chatStatus", "away");
    currentStatus = "away";
  } else {
    $.get("http://" + domain + "/eChat/status/setStatus?status=online");
    localStorage.setItem("chatStatus", "online");
    currentStatus = "online";
  }
}

$(document).off("click", "a.changeStatus").on("click", "a.changeStatus", function () {
  var cStatus = $(this).attr("data-status");
  if (cStatus != "dnd") {
    $(this).attr("data-status", "dnd");
    changeStatus("dnd");
  } else {
    $(this).attr("data-status", "online");
    changeStatus("online");

  }
});

$(window).blur(function () {
  if (currentStatus != "dnd") {
    changeStatus("away");
  }
})

$(window).focus(function () {
  if (currentStatus != "dnd") {
    changeStatus("online");
  }
})
function setUserStatus(ui, status) {
  $(ui).each(function () {
    if (status == "dnd") {
      $(this).parent().find("a.startChat").hide();
      $(this).attr("data-original-title", "Set Online");
      $(this).attr("data-status", "dnd");
      $(this).find("i").last().alterClass("fa-* txt-color-*", "fa-times-circle txt-color-red");
      $(this).find(".fa-stack i").first().alterClass("txt-color-*", "fa-stack-2x txt-color-red").next().alterClass("txt-color-*", "fa-stack-1x txt-color-white");
    } else if (status == "away") {
      $(this).parent().find("a.startChat").show();
      $(this).attr("data-original-title", "Set DND");
      $(this).attr("data-status", "away");
      $(this).find("i").last().alterClass("fa-* txt-color-*", "fa-clock-o txt-color-orange");
      $(this).find(".fa-stack i").first().alterClass("txt-color-*", "fa-stack-2x txt-color-orange").next().alterClass("txt-color-*", "fa-stack-1x txt-color-white");
    } else {
      $(this).parent().find("a.startChat").show();
      $(this).attr("data-original-title", "Set DND");
      $(this).find("i").last().alterClass("fa-* txt-color-*", "fa-check-circle txt-color-greenLight");
      $(this).find(".fa-stack i").first().alterClass("txt-color-*", "fa-stack-2x txt-color-greenLight").next().alterClass("txt-color-*", "fa-stack-1x txt-color-white");
      $(this).attr("data-status", "online");
    }
  });
}

function renderMember(member) {
  // console.log(member);
  var statusLink;
  if (member.info.status == "online") {
    if (member.info.contactid == $("#userID").val()) {
      statusLink = '<a rel="tooltip" data-original-title="Set DND" data-placement="left" href="##" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="online"><i class="fa fa-check-circle txt-color-greenLight"></i></a>';
    } else {
      statusLink = '<span rel="tooltip" data-original-title="User is available" data-placement="left" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="online"><i class="fa fa-check-circle txt-color-greenLight"></i></span>';
    }
  } else if (member.info.status == "away") {
    if (member.info.contactid == $("#userID").val()) {
      statusLink = '<a rel="tooltip" data-original-title="Set DND" data-placement="left" href="##" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="away"><i class="fa fa-clock-o txt-color-orange"></i></a>';
    } else {
      statusLink = '<span rel="tooltip" data-original-title="User is away" data-placement="left" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="away"><i class="fa fa-clock-o txt-color-orange"></i></span>';
    }
  } else {

    if (member.info.contactid == $("#userID").val()) {
      statusLink = '<a rel="tooltip" data-original-title="Set Online" data-placement="left" href="##" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="dnd"><i class="fa fa-times-circle txt-color-red"></i></a>';
    } else {
      statusLink = '<span rel="tooltip" data-original-title="User is unavailable" data-placement="left" data-userID = "' + member.info.contactid + '" class="pull-right changeStatus" data-status="dnd"><i class="fa fa-times-circle txt-color-red"></i></span>';
    }
  }
  var chatLink = "";
  if (member.info.contactid != $("#userID").val()) {
    chatLink = '<a rel="tooltip" data-original-title="Start Chat" data-placement="left" href="#" data-userEmail="' + member.info.email + '" data-userID = "' + member.info.contactid + '" class="pull-right user_' + member.info.contactid + ' startChat" style="display:' + (member.info.status != "dnd" ? "inherit" : "none") + '" data-userName="' + member.info.name + '"><i class="fa fa-comments-o"></i></a>'
  }
  var logUsers = $('#offUsers').val();
  if (logUsers.length) {
        $('#offUsers').val(logUsers + ',' + member.info.contactid);
      } else {
        $('#offUsers').val(member.info.contactid);
      }
  $('.uList').append(
      '<li class="userList draggable" data-userID="' + member.info.contactid + '">' +
      '<img src="http://' + domain + '/eunify/avatar?contactID=' + member.info.contactid + '&size=50" height="50" width="50" class="img-thumbnail img-circle">' +
      '<div class="uName">' +
      '<h4>' +
      '<a href="#">' + member.info.name + '</a>' +
      statusLink +
      chatLink +
      '</h4>' +
      '<p>' + member.info.company + '</p>' +
      '</div>' +
      '</li>'
  );
}

$(".draggable").on("click", function () {
  $(this).draggable({appendTo: "body", revert: "invalid", helper: "clone", zIndex: 5000});
})
$(".droppable").on("click", function () {
  $(this).droppable({
    activeClass: "alert alert-block alert-warning",
    hoverClass: "alert alert-success alert-block",
    drop: function (event, ui) {
      var a = $(ui.draggable).find("a.startChat");
      var newUserID = $(a).attr("data-userID");
      var newUserName = $(a).attr("data-userName");
      var newUserEmail = $(a).attr("data-userEmail");
      var chatWindow = $(event.target);
      var chatHash = $(chatWindow).attr("id");
      var existingChat = localStorage.getObject("chatWindows");
      var thisChat = existingChat[chatHash];
      var currentChatUsers = thisChat.chatUsers;
      var currentChatUserNames = thisChat.userNames;
      var currentChatUserIDs = thisChat.chatUserIDs;
      if ($.inArray(newUserEmail, currentChatUsers) == -1) {
        // add the new user to the chat...
        currentChatUsers.push(newUserEmail);
        currentChatUserNames.push(newUserName);
        currentChatUserIDs.push(newUserID);
        currentChatUsers.sort();
        currentChatUserNames.sort();
        var newHash = $.md5(currentChatUsers.join(", "));
        thisChat.chatUsers = currentChatUsers;
        thisChat.chatUsers = currentChatUsers;
        thisChat.chatUserIDs = currentChatUserIDs;
        delete existingChat[chatHash];
        existingChat[newHash] = thisChat;
        localStorage.setObject("chatWindows", existingChat);
        // notify other users (possibly)
        $.ajax({
          url: "http://" + domain + "/eChat/chat/addUser",
          data: {
            channelData: JSON.stringify(thisChat),
            newUser: JSON.stringify({
              userName: newUserName,
              userEmail: newUserEmail,
              userID: newUserID
            }),
            oldHash: chatHash,
            newHash: newHash,
            socketID: socketID
          },
          method: "POST"
        });
        //rename id on current element
        $(chatWindow).attr("id", newHash);
        var chatTab = $("#chatTabs").find("a[data-chatID='" + chatHash + "']");
        // console.log(chatTab);
        var chatText = thisChat.userNames;
        // console.log(chatText);
        chatText.splice($.inArray($("#userName").val(), chatText), 1);
        $(chatTab).attr("href", "#" + newHash).attr("data-chatID", newHash).find("span").text(chatText.join(", "));
      }

    }
  });
})
function setupPusher(teams) {

  presenceChannel = pusher.subscribe("presence-" + site_id);
  presenceChannel.bind("pusher:subscription_succeeded", function (members) {
    socketID = members.members[members.me.id].socketID;
    $("#currentUserCount").text(members.count);
    $("#sidebar .userList").empty().append("<ul class='uList'></ul>");
    members.each(
        function (member) {
          $.get("http://" + domain + "/eChat/status/getStatus?contactID=" + member.id, function (data) {
            member.info.status = data;
            renderMember(member);
          });
        }
    );
  });
  presenceChannel.bind("statusEvent", function (chatData) {
    // console.log(chatData);
    var user = $(".changeStatus[data-userID='" + chatData.userID + "']");
    // console.log(user);
    var currentStatus = chatData.status;
    setUserStatus(user, currentStatus);
  })
  presenceChannel.bind("pusher:member_added", function (member) {
    console.log(member);
    $("#currentUserCount").text(parseInt($("#currentUserCount").text()) + 1);
    renderMember(member);
  });
  presenceChannel.bind("pusher:member_removed", function (member) {
    $("#currentUserCount").text(parseInt($("#currentUserCount").text()) - 1);
    $(".uList li[data-userID=" + member.info.contactid + "]").remove();
  });

  for (var i = 0; i < teams.length; i++) {
    var teamName = teams[i]["NAME"];
    var chatChannelName = teams[i]["SITEID"];
    var chatChannel = pusher.subscribe("private-team-" + teams[i]["ID"] + '_' + site_id);
    chatChannel.bind("pusher:subscription_succeeded", function (members) {
    });
    chatChannel.bind("typingEvent", function (chatData) {
      // console.log(chatData);
      var thisChat = chatData.chatHash;
      if (chatData.userID != $("#userID").val()) {
        var typingData = $("#" + thisChat + " .userTyping").data("currentlyTyping");
        if (typingData == null) {
          typingData = [];
        }
        // console.log("typingData: " + typingData);
        if (chatData.isTyping == "true") {
          // see if the user exists in the typing message
          if ($.inArray(chatData.userName, typingData) < 0) {
            typingData.push(chatData.userName);
          }
          $("#" + thisChat + " .userTyping").data("currentlyTyping", typingData);
          if (typingData.length > 1) {
            $("#" + thisChat + " .userTyping").html(typingData + " are typing...");
          } else {
            $("#" + thisChat + " .userTyping").html(chatData.userName + " is typing...");
          }
        } else {
          if (typingData != null && $.inArray(chatData.userName, typingData) >= 0) {
            typingData.splice($.inArray(chatData.userName, typingData), 1);
            if (typingData.length > 1) {
              $("#" + thisChat + " .userTyping").html(typingData + " are typing...");
            } else if (typingData.length > 0) {
              $("#" + thisChat + " .userTyping").html(typingData[0] + " is typing...");
            } else {
              $("#" + thisChat + " .userTyping").empty();
            }
            $("#" + thisChat + " .userTyping").data("currentlyTyping", typingData);
          }

        }
      }
    });
    chatChannel.bind("addChatUser", function (chatData) {
      var existingChat = localStorage.getObject("chatWindows")
      if (existingChat.hasOwnProperty(chatData.oldHash)) {
        var thisChat = existingChat[chatData.oldHash];
        thisChat.chatUsers = chatData.channelData.chatUsers;
        thisChat.userNames = chatData.channelData.userNames;
        thisChat.userIDs = chatData.channelData.userIDs;
        delete existingChat[chatData.oldHash];
        existingChat[chatData.newHash] = thisChat;
        localStorage.setObject("chatWindows", existingChat);
        // notify other users (possibly)
        var chatWindow = $("#" + chatData.oldHash);
        $(chatWindow).attr("id", chatData.newHash);
        var chatTab = $("#chatTabs").find("a[data-chatID='" + chatData.oldHash + "']");
        var chatText = thisChat.userNames;
        chatText.splice($.inArray($("#userName").val(), chatText), 1);
        $(chatTab).attr("href", "#" + chatData.newHash).attr("data-chatID", chatData.newHash).find("span").text(chatText.join(", "));
      }

    });
    chatChannel.bind("chatMessage", function (chatData) {

      console.log(chatData);
      if (currentStatus != "dnd") {
        var thisChat = chatData.chatHash;
        // console.log(chatData);
        var existingChat = localStorage.getObject("chatWindows");
        var chatExists = existingChat.hasOwnProperty(thisChat);
        var currentUserStatus = $(".uList a[data-userID='" + chatData.userID + "']").attr("data-status");
		
        if (chatExists) {
			showNotification(existingChat, chatData, false);
          if (currentStatus == "away" && chatData.messageType != "link") {
            //$.flashTitle("New Message from " + chatData.userName);

            var audioElement = document.createElement('audio');

            if (navigator.userAgent.match('Firefox/'))
              audioElement.setAttribute('src', $.sound_path + "bigbox.ogg");
            else
              audioElement.setAttribute('src', $.sound_path + "bigbox.mp3");

            //$.get();
            audioElement.addEventListener("load", function () {
              audioElement.play();

            }, true);

            audioElement.pause();
            audioElement.play();
			//showNotification(existingChat, chatData, false);
            
          }
		  
          var userIcon = $('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i></span>');
          var icon = $("<i>");
          if (status == "dnd") {
            $(icon).addClass("fa fa-times-circle txt-color-white fa-stack-1x");
            $(userIcon).find("i").addClass("txt-color-red");
          } else if (status == "away") {
            $(icon).addClass("fa fa-clock-o txt-color-white fa-stack-1x");
            $(userIcon).find("i").addClass("txt-color-orange");
          } else {
            $(icon).addClass("fa fa-check-circle txt-color-white fa-stack-1x");
            $(userIcon).find("i").addClass("txt-color-greenLight");
          }
          $(userIcon).append(icon);
          console.log(chatData);
          if (chatData.messageType == "message") {
            var chatItem = $(
                '<div data-timestamp="' + chatData.timestamp + '" class="media chatItem">' +
                '<div class="media-left">' +
                '<a href="#">' +
                '<img width="50" class="media-object img-thumbnail img-rounded" src="http://' + domain +'/eunify/avatar?contactID=' + chatData.userID + '&size=50"/>' +
                '<span class="changeStatus" data-userID="' + chatData.userID + '"></span>' +
                '</a>' +
                '</div>' +
                '<div class="media-body">' +
                '<h4 class="media-heading handle">' + chatData.userName + ' <span class="timestamp">' + moment(chatData.timestamp).format("ddd H:MM") + '</span></h4>' +
                '<p>' + chatData.message + '</p>' +
                '</div>' +
                '</div>'
            );
            $(chatItem).find("span.changeStatus").append(userIcon);
            $("#" + thisChat + " .chatHistory").append(chatItem).emoticonize();
          } else if (chatData.messageType == "link" || chatData.messageType == "attachment") {
            var chatItem = $(
                '<div data-timestamp="' + chatData.timestamp + '" class="media chatItem">' +
                '<div class="media-left"></div>' +
                '<div class="media-body">' +
                '<p>' + chatData.message + '</p>' +
                '</div>' +
                '</div>'
            );
            $("#" + thisChat + " .chatHistory").append(chatItem);
          }

          //$("#" + thisChat + " .chatHistory").scrollbox("update");
          var itemHeight = $("#" + thisChat + " .chatHistory .chatItem").last().innerHeight();
          $("#" + thisChat + " .chatHistory").scrollTop($("#" + thisChat + " .chatHistory")[0].scrollHeight);
          //$("#" + thisChat + " .chatHistory").scrollbox("scroll", itemHeight + 70);
          // if it's not visible, give them a notification

          if (!$(".chatWindow").is(":visible") && chatData.messageType != "link" && currentStatus == "online") {
            $.smallBox({
              title: chatData.userName,
              content: chatData.message,
              color: "#296191",
              //timeout: 8000,
              icon: "fa fa-commenting swing animated"
            }, function () {
              $(".chatWindow").show();
            })
          }
          checkActiveTab(thisChat);
        } else {
          console.log("No existing chat: " + thisChat + " " + JSON.stringify(chatData));
          if (chatData.messageType != "link") {
            var requestedChatData = {
              email: chatData.channelData.email,
              userNames: chatData.channelData.userNames,
              chatUsers: chatData.channelData.chatUsers,
              chatUserIDs: chatData.channelData.chatUserIDs,
              chatChannel: chatData.channelData.chatChannel,
              teamName: chatData.channelData.teamName
            }
            showNotification(requestedChatData, chatData, true);

          }
        }
      }
    });
    chatChannels.push(chatChannel);
  }
  var chatChannel = pusher.subscribe("private-chat-" + site_id);
  chatChannel.bind("pusher:subscription_succeeded", function (members) {
  });
  chatChannel.bind("typingEvent", function (chatData) {
    // console.log(chatData);
    var thisChat = chatData.chatHash;
    if (chatData.userID != $("#userID").val()) {
      var typingData = $("#" + thisChat + " .userTyping").data("currentlyTyping");
      if (typingData == null) {
        typingData = [];
      }
      // console.log("typingData: " + typingData);
      if (chatData.isTyping == "true") {
        // see if the user exists in the typing message
        if ($.inArray(chatData.userName, typingData) < 0) {
          typingData.push(chatData.userName);
        }
        $("#" + thisChat + " .userTyping").data("currentlyTyping", typingData);
        if (typingData.length > 1) {
          $("#" + thisChat + " .userTyping").html(typingData + " are typing...");
        } else {
          $("#" + thisChat + " .userTyping").html(chatData.userName + " is typing...");
        }
      } else {
        if (typingData != null && $.inArray(chatData.userName, typingData) >= 0) {
          typingData.splice($.inArray(chatData.userName, typingData), 1);
          if (typingData.length > 1) {
            $("#" + thisChat + " .userTyping").html(typingData + " are typing...");
          } else if (typingData.length > 0) {
            $("#" + thisChat + " .userTyping").html(typingData[0] + " is typing...");
          } else {
            $("#" + thisChat + " .userTyping").empty();
          }
          $("#" + thisChat + " .userTyping").data("currentlyTyping", typingData);
        }

      }
    }
  });
  chatChannel.bind("addChatUser", function (chatData) {
    var existingChat = localStorage.getObject("chatWindows")
    if (existingChat.hasOwnProperty(chatData.oldHash)) {
      var thisChat = existingChat[chatData.oldHash];
      thisChat.chatUsers = chatData.channelData.chatUsers;
      thisChat.userNames = chatData.channelData.userNames;
      thisChat.userIDs = chatData.channelData.userIDs;
      delete existingChat[chatData.oldHash];
      existingChat[chatData.newHash] = thisChat;
      localStorage.setObject("chatWindows", existingChat);
      // notify other users (possibly)
      var chatWindow = $("#" + chatData.oldHash);
      $(chatWindow).attr("id", chatData.newHash);
      var chatTab = $("#chatTabs").find("a[data-chatID='" + chatData.oldHash + "']");
      var chatText = thisChat.userNames;
      chatText.splice($.inArray($("#userName").val(), chatText), 1);
      $(chatTab).attr("href", "#" + chatData.newHash).attr("data-chatID", chatData.newHash).find("span").text(chatText.join(", "));
    }

  });
  chatChannel.bind("chatMessage", function (chatData) {
    console.log(chatData);
    if ($.inArray($("#userEmail").val(), chatData.channelData.chatUsers) > -1 && currentStatus != "dnd") {
      var thisChat = chatData.chatHash;
      // console.log(chatData);
      var existingChat = localStorage.getObject("chatWindows");
      var chatExists = existingChat.hasOwnProperty(thisChat);
      var currentUserStatus = $(".uList a[data-userID='" + chatData.userID + "']").attr("data-status");
		
      if (chatExists) {
		showNotification(existingChat, chatData, false);  
        console.log(currentStatus);
        if (currentStatus == "away" && chatData.messageType != "link") {
          // $.flashTitle("New Message from " + chatData.userName);

          var audioElement = document.createElement('audio');

          if (navigator.userAgent.match('Firefox/'))
            audioElement.setAttribute('src', $.sound_path + "bigbox.ogg");
          else
            audioElement.setAttribute('src', $.sound_path + "bigbox.mp3");

          //$.get();
          audioElement.addEventListener("load", function () {
            audioElement.play();
          }, true);

          audioElement.pause();
          audioElement.play();

          //showNotification(existingChat, chatData, false);
        }
        var userIcon = $('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i></span>');
        var icon = $("<i>");
        if (status == "dnd") {
          $(icon).addClass("fa fa-times-circle txt-color-white fa-stack-1x");
          $(userIcon).find("i").addClass("txt-color-red");
        } else if (status == "away") {
          $(icon).addClass("fa fa-clock-o txt-color-white fa-stack-1x");
          $(userIcon).find("i").addClass("txt-color-orange");
        } else {
          $(icon).addClass("fa fa-check-circle txt-color-white fa-stack-1x");
          $(userIcon).find("i").addClass("txt-color-greenLight");
        }
        $(userIcon).append(icon);
        if (chatData.messageType == "message") {
          if ($("#" + thisChat + " .chatHistory .chatItem").last().find(".changeStatus").attr("data-userid") == chatData.userID || moment($("#" + thisChat + " .chatHistory .chatItem").last().attr("data-timestamp")).isAfter(moment(chatData.timestamp).subtract(5,"minutes")) ) {
              // just append to the next bit
              var chatItem = $('<p data-timestamp="' + chatData.timestamp + '" data-userID="' + chatData.userID + '">' + chatData.message + '</p>');
              $("#" + thisChat + " .chatHistory .chatItem").last().find(".media-body").append(chatItem).emoticonize();
            } else {
              var chatItem = $(
                  '<div data-timestamp="' + chatData.timestamp + '" class="media chatItem">' +
                  '<div class="media-left">' +
                  '<a href="#">' +
                  '<img width="50" class="media-object img-thumbnail img-rounded" src="/eunify/avatar?contactID=' + chatData.userID + '&size=50"/>' +
                  '<span class="changeStatus" data-userID="' + chatData.userID + '"></span>' +
                  '</a>' +
                  '</div>' +
                  '<div class="media-body">' +
                  '<h4 class="media-heading handle">' + chatData.userName + ' <span class="timestamp">' + moment(chatData.timestamp).format("ddd H:MM") + '</span></h4>' +
                  '<p data-timestamp="' + chatData.timestamp + '" data-userID="' + chatData.userID + '">' + chatData.message + '</p>' +
                  '</div>' +
                  '</div>'
              );
              $(chatItem).find("span.changeStatus").append(userIcon);
              $("#" + thisChat + " .chatHistory").append(chatItem).emoticonize();
            }
        } else if (chatData.messageType == "link" || chatData.messageType == "attachment") {
          var chatItem = $(
              '<div data-timestamp="' + chatData.timestamp + '" class="media chatItem">' +
              '<div class="media-left"></div>' +
              '<div class="media-body">' +
              '<p>' + chatData.message + '</p>' +
              '</div>' +
              '</div>'
          );
          $("#" + thisChat + " .chatHistory").append(chatItem);
        }

        //$("#" + thisChat + " .chatHistory").scrollbox("update");
        var itemHeight = $("#" + thisChat + " .chatHistory .chatItem").last().innerHeight();
        $("#" + thisChat + " .chatHistory").scrollTop($("#" + thisChat + " .chatHistory")[0].scrollHeight);
        //$("#" + thisChat + " .chatHistory").scrollbox("scroll", itemHeight + 70);
        // if it's not visible, give them a notification

        if (!$(".chatWindow").is(":visible") && chatData.messageType != "link" && currentStatus == "online") {
          $.smallBox({
            title: chatData.userName,
            content: chatData.message,
            color: "#296191",
            //timeout: 8000,
            icon: "fa fa-commenting swing animated"
          }, function () {
            $(".chatWindow").show();
          })
        }
        checkActiveTab(thisChat);
      } else {
        console.log("No existing chat: " + thisChat + " " + JSON.stringify(chatData));
        if (chatData.messageType != "link") {
          var requestedChatData = {
            email: chatData.channelData.email,
            userNames: chatData.channelData.userNames,
            chatUsers: chatData.channelData.chatUsers,
            chatUserIDs: chatData.channelData.chatUserIDs,
            chatChannel: chatData.channelData.chatChannel,
            teamName: ""

          }
          showNotification(requestedChatData, chatData, true);

        }
      }
    }
  });
  chatChannels.push(chatChannel);

}

function timerIncrement() {
  idleTime += 1;
  if (currentStatus != "dnd") {
    if (idleTime >= 3) { // 3 mins
      if (currentStatus != "away") {
        changeStatus("away");
      }
    } else {
      if (document.hasFocus()) {
        changeStatus("online");
      }
    }
  }
}

$(document).off("click", ".closeChat").on("click", ".closeChat", function () {
  $(".chatWindow").toggle();
})
$("textarea.chatText").on("click", function () {
  autosize(this);
});
$(document).off("click", ".closeChatTab").on("click", ".closeChatTab", function (e) {
  var thisLink = $(this).parent();
  var thisTab = this;
  var tabID = $(thisLink).attr("data-chatID");
  var prevTab = $(this).closest("li").prev(".realTab").children("a");
  if (!prevTab.length || prevTab == undefined) {
    prevTab = $(this).closest("li").next(".realTab").children("a");
  } else {
    // console.log(prevTab);
  }
  /*$.SmartMessageBox({
   title : "Close tab",
   content : "Close this chat tab?",
   buttons : '[No][Yes]'
   }, function(ButtonPressed) {
   if (ButtonPressed === "Yes") {*/
  $(thisTab).closest("li").remove();
  $("#" + tabID).remove();

  var existingChat = localStorage.getObject("chatWindows");
  delete existingChat[tabID];
  localStorage.setObject("chatWindows", existingChat);
  if ($("#chatTabs .realTab").length) {
    $(prevTab).tab("show");
  } else {
    $(".chatWindow").toggle();
  }
  /*}
   });*/
  return false;
})

$(document).off("keypress", "textarea.chatText").on("keypress", "textarea.chatText", function (e) {
  // Enter was pressed without shift key
  if (e.keyCode == 13 && !e.shiftKey) {
    var chatHash = $(this).closest(".tab-pane").attr("id");
    var teamID = $(this).closest(".tab-pane").attr("data-teamID");
    var thisE = $(this);
    e.preventDefault();
    clearTimeout($(this).data("timer"));
    $.get(
        "http://" + domain + "/eChat/chat/userTyping",
        {
          isTyping: false,
          chatHash: chatHash
        }
    );
    if ($(this).closest(".tab-pane").find(".chatHistory").is(":empty")) {

      $.ajax({
        url: "http://" + domain + "/eChat/chat/sendChat",
        data: {
          userName: $("#userName").val(),
          channelData: JSON.stringify(localStorage.getObject("chatWindows")[chatHash]),
          message: $(thisE).val(),
          teamID: teamID,
          chatHash: chatHash
        },
        method: "POST",
        success: function () {
          $(thisE).val("").focus();
        }
      });
      $(thisE).val("");
    } else {
      if (!$(this).val().length) {
        return;
      }
      $.ajax({
        url: "http://" + domain + "/eChat/chat/sendChat",
        data: {
          userName: $("#userName").val(),
          channelData: JSON.stringify(localStorage.getObject("chatWindows")[chatHash]),
          message: $(this).val(),
          teamID: teamID,
          chatHash: chatHash
        },
        method: "POST",
        success: function () {
          $(this).val("").focus();
        }
      });
      $(this).val("");
    }


  }
});

$(document).off("keydown", "textarea.chatText").on("keydown", "textarea.chatText", function (e) {
  // Clear any "stop" timer for typing. This way,
  // the previous stop event doesn't get triggered
  // while the user has continued to type.
  clearTimeout($(this).data("timer"));
  // Check to see if the user is currently typing.
  // If they are, then we don't need to do any of
  // this stuff until they stop.
  if ($(this).data("isTyping")) {
    return;
  }
  // At this point, we know the user was not
  // previously typing so we can send the request
  // to the server that the user has started.
  $(this).data("isTyping", true);
  var chatHash = $(this).closest(".tab-pane").attr("id");
  // Tell the server that this user is typing.
  $.get(
      "http://" + domain + "/eChat/chat/userTyping",
      {
        isTyping: true,
        chatHash: chatHash
      }
  );
});

$(document).off("blur", "textarea.chatText").on("blur", "textarea.chatText", function (e) {
  clearTimeout($(this).data("timer"));
  $(this).data("isTyping", false);
  var chatHash = $(this).closest(".tab-pane").attr("id");
  $.get(
      "http://" + domain + "/eChat/chat/userTyping",
      {
        isTyping: false,
        chatHash: chatHash
      }
  );
})
function checkActiveTab(chatHash) {
  var activeTab = $("ul#chatTabs li.active a");
  if ($(activeTab).attr("data-chatID") == chatHash) {
    return;
  } else {
    $("ul#chatTabs a[data-chatID='" + chatHash + "']").addClass("chatTabFlash");
  }
}

$(document).off("keyup", "textarea.chatText").on("keyup", "textarea.chatText", function (e) {
  // Clear any "stop" timer for typing. We need to
  // clear here as well because it looks like the
  // browser has trouble trapping every single
  // individual key as a different typing event
  // (at least, that's what I think is going on).
  clearTimeout($(this).data("timer"));
  // The key up event doesn't mean that the user
  // has stopped typing. But, it does give us a
  // reason to start paying attention. Let's check
  // back shortly.
  var thisE = this;
  var chatHash = $(this).closest(".tab-pane").attr("id");
  $(this).data(
      "timer",
      setTimeout(
          function () {
            // Flag that the user is no longer
            // typing a message.
            $(thisE).data("isTyping", false);
            // Tell the server that this user
            // has stopped typing.
            $.get(
                "http://" + domain + "/eChat/chat/userTyping",
                {
                  isTyping: false,
                  chatHash: chatHash
                }
            );
          },
          2000
      )
  );
});


$(document).off("click", ".chatTabFlash").on("click", ".chatTabFlash", function () {
  $(this).removeClass("chatTabFlash");
})
$(window).resize(function () {
  resizeWindow();
})
$(document).off("click", ".startChat").on("click", ".startChat", function startChat() {
  $('#content-team').empty();
  var width = document.body.clientWidth;
	console.log(width);
	if(width < 570){
		$('#chatTeams').hide();
		$('#chatUserList').hide();
		$('.right-sidebar').css('background','#fff');
		$('.right-sidebar').show();
	}
  var userEmail = $(this).attr("data-useremail");
  var myEmail = $("#userEmail").val();
  var myUserName = $("#userName").val();
  var myUserID = $("#userID").val();
  var userID = $(this).attr("data-userID");
  var userName = $(this).attr("data-userName");
  var existingChat = localStorage.getObject("chatWindows");
  var chatUsers = [myEmail, userEmail];
  var chatUsersIDs = [myUserID, userID];
  var teamName = $(this).attr("data-teamName");
  var teamID = $(this).attr("data-teamid");
  chatUsers.sort();
  var chatChannel = $(this).attr("data-channel");
  if (chatChannel == undefined) {
    var thisChat = $.md5(chatUsers.join());
    chatChannel = "private-chat-" + site_id
  } else {
    var thisChat = $.md5(chatChannel);

  }

  var chatExists = existingChat.hasOwnProperty(thisChat);

  if (!chatExists) {

    existingChat[thisChat] = {
      email: userEmail,
      userNames: [
        myUserName,
        userName
      ],
      chatUserIDs: chatUsersIDs,
      chatUsers: chatUsers,
      chatChannel: chatChannel,
      teamName: teamName
    }
    localStorage.setObject("chatWindows", existingChat);
  } else {
    $("a[data-chatID=" + thisChat + "]").tab("show")
  }
  if (!$(".chatWindow").is("visible")) {

    $(".chatWindow").show();
  }
  //
  if (!chatExists) {
    if (teamName != "" && teamName != undefined) {
      var chatTitle = teamName;
    } else {
      var chatTitle = userName;
    }
    // console.log(localStorage.getObject("chatWindows"));
    $.ajax({
      url: "http://" + domain + "/eChat/archive/history?chatHash=" + thisChat,
      success: function (data) {
        var data = data.replace(new RegExp("/eunify/avatar?", "g"), 'http://' + domain + '/eunify/avatar?');
        //console.log(data);
        $("#chatWindows").append(
            '<div class="droppable tab-pane fade in" id="' + thisChat + '" data-teamID="' + teamID + '">' +
            createChatMore(thisChat) +
            '<div class="chatHistory"></div>' +
            createChatTextBox(userID, userName, thisChat) +
            '</div>'
        );
        $("#chatWindows #" + thisChat + " .chatHistory").append($(data).find('.chatItem'));
        resizeWindow();
        //$("#chatWindows .chatHistory").last().scrollbox("update");
        $("#chatTabs").append(
            '<li class="realTab">' +
            '<a data-chatID="' + thisChat + '" href="#" data-target="#' + thisChat + '" data-teamID = "' + teamID + '" data-toggle="tab"><span>' + chatTitle + '</span> <i data-container="body" data-placement="right" data-original-title="Close Chat Tab" rel="tooltip" class="fa fa-times-circle closeChatTab"></i></a>' +
            '</li>'
        );
        $("#chatTabs a:last").tab("show");
        $(".chatText").attr('data-TeamID', teamID);
        $("#" + thisChat + " .chatHistory").scrollTop($("#" + thisChat + " .chatHistory")[0].scrollHeight);
      }
    });
    if (typeof teamName == 'undefined') {
      var type = 'chat';
    } else {
      var type = 'team';
    }
    $.ajax({
      url: "http://" + domain + "/eChat/chat/seenChat",
      data: {
        chathash: thisChat
      },
      method: "POST",
      success: function () {
        // $(thisE).val("").focus();
      }
    });


  }

});
$(document).off("keypress", ".searchChatHistory").on("keypress", ".searchChatHistory", function (e) {
  if (e.keyCode == 13 && !e.shiftKey) {
    e.preventDefault();
    var thisE = this;
    var chatHash = $(this).closest(".tab-pane").attr("id");
    var chatHistory = $(this).closest(".droppable").find(".chatHistory");

    $.ajax({
      url: "http://" + domain + "/eChat/archive/history",
      method: "POST",
      data: {
        chatHash: chatHash,
        keywords: $(thisE).val()
      },
      success: function (data) {
        var data = data.replace(new RegExp("/eunify/avatar?", "g"), 'http://' + domain + '/eunify/avatar?');
        $(chatHistory).empty();
        $(chatHistory).html($(data).find('.chatItem'));
        resizeWindow();
        //$(chatHistory).scrollbox("update");
      }
    });
  }
  return;

})
$(document).off("click", ".showMoreHistory").on("click", ".showMoreHistory", function () {
  var chatHash = $(this).closest(".tab-pane").attr("id");
  var chatHistory = $(this).closest(".droppable").find(".chatHistory");
  var lastTimeStamp = $(chatHistory).find(".chatItem").first().attr("data-timestamp");
  if (lastTimeStamp == undefined) {
    lastTimeStamp = moment().format();
  }
  $.ajax({
    url: "http://" + domain + "/eChat/archive/history?chatHash=" + chatHash + "&timestamp=" + encodeURIComponent(lastTimeStamp),
    success: function (data) {
      var data = data.replace(new RegExp("/eunify/avatar?", "g"), 'http://' + domain + '/eunify/avatar?');
      $(chatHistory).prepend($(data).find('.chatItem'));
      resizeWindow();
      //$(chatHistory).scrollbox("update");
    }
  });
})
function createChatMore(thisChat) {
  // load all of today's conversations...
  var chatMore =
      '<div class="chatMore">' +
      '<form class="">' +
      '<div class="row">' +
      '<div class="col-sm-12">' +
      '<div class="input-group">' +
      '<span class="input-group-addon" id="sizing-addon2"><i class="icon-prepend fa fa-search"></i></span>' +
      '<input type="text" class="form-control searchChatHistory" placeholder="Search Chat History">' +
      '<span class="input-group-btn">' +
      '<a data-container="body" data-placement="left" data-original-title="Show more chat history" rel="tooltip" href="#" class="showMoreHistory btn btn-default"><i class="fa fa-history"></i></a>' +
      '</span>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</form>' +
      '</div>';

  return chatMore;
}

function createChatTextBox(userID, userName, chatHash) {
  return (
  '<div class="chatText form-group">' +
  '<div class="userTyping"></div>' +
  '<div class="chatBox">' +
  '<div class="chatControls">' +
  '<a href="#" class="addFile btn btn-primary"><i class="fa fa-paperclip"></i></a>' +
  '</div>' +
  '<div class="chatTextBox">' +
  '<textarea placeholder="Start chatting!" rows="1" data-User="' + userName + '" data-UserID = "' + userID + '" onfocus="seenMessage(this)" class="form-control chatText"></textarea>' +
  '</div>' +
  '<form action="http://' + domain + '/eChat/attachment/send" method="post" enctype="multipart/form-data"  style="visibility: hidden; width: 1px; height: 1px">' +
  '<input id="upload-' + chatHash + '" data-userName="' + userName + '" data-userID="' + userID + '" type="file" name="chatFile" class="chatFile">' +
  '</form>' +
  '</div>' +
  '</div>');
}

$(document).off("click", ".addFile").on("click", ".addFile", function () {
  $(this).closest(".chatBox").find(".chatFile").trigger("click");
})
$(document).off("change", "input.chatFile").on("change", "input.chatFile", function () {
  if ($(this).val() != "") {
    var thisElement = this;
    var chatHash = $(this).closest(".tab-pane").attr("id");
    var userID = $(this).attr("data-userID");
    var userName = $(this).attr("data-userID");
    $(this).closest("form").ajaxSubmit({
      dataType: 'json',
      data: {
        chatHash: chatHash,
        userID: userID,
        userName: userName,
        channelData: JSON.stringify(localStorage.getObject("chatWindows")[chatHash])
      },
      beforeSend: function () {
        $(thisElement).closest(".chatBox").find("a").addClass("btn-danger").removeClass("btn-primary").find("i").addClass("fa-circle-o-notch fa-spin").removeClass("fa-paperclip");
      },
      complete: function (xhr) {
        $(thisElement).closest(".chatBox").find("a").removeClass("btn-danger").addClass("btn-primary").find("i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-paperclip");
      }
    });
  }
})


$(this).mousemove(function (e) {
  if (idleTime >= 3 && document.hasFocus() && currentStatus != "dnd") {
    changeStatus("online");
  }
  idleTime = 0;
});
$(this).keypress(function (e) {
  if (idleTime >= 3 && document.hasFocus() && currentStatus != "dnd") {
    changeStatus("online");
  }
  idleTime = 0;
});
window.loadTeamDataJS = function () {
  $.ajax({
    url: 'http://' + domain + '/eChat/teams/getTeams',
    data: {'created_by': currentUser},
    method: "post",
    async: false,
    success: function (obj) {
      //var obj = jQuery.parseJSON(data);
      setupPusher(obj);
      if (obj.length > 0) {
        $('.teamList').html('');
        for (var i = 0; i < obj.length; i++) {
          $('.teamList').append(
              '<li class="team_List draggable" data-teamID="' + obj[i]["ID"] + '">' +
              '<i class="fa fa-users"></i>' +
              '<div class="teamName">' +
              '<h4>' +
              '<a href="#" class="startChat" data-teamName="' + obj[i]["NAME"] + '" data-teamID="' + obj[i]["ID"] + '" data-channel="private-team-' + obj[i]["ID"] + '_' + site_id + '">' + obj[i]["NAME"] + '</a>' +
              ((obj[i]["CREATED_BY"] == currentUser) ? '<a rel="tooltip" title="Edit this team" data-placement="left" data-container="body" href="#" class="noAjax team_add pull-right" name="Edit Team" data-TeamID = "' + obj[i]["ID"] + '"><i class="fa fa-pencil-square"></i></a>' : '') +
              ((obj[i]["CREATED_BY"] == currentUser) ? '<a rel="tooltip" id=' + obj[i]["ID"] + ' title="Delete this team" data-placement="left" href="#" data-id=' + obj[i]["ID"] + ' onclick="deleteTeam(this);" class="pull-right" name="' + obj[i]["NAME"] + '" ><i class="fa fa-times"></i></a>' : '') +
              ((obj[i]["CREATED_BY"] == currentUser) ? '' : '<a rel="tooltip" id=' + obj[i]["ID"] + ' title="Leave this team" data-placement="left" href="#" data-id=' + obj[i]["ID"] + ' onclick="leaveGroup(this);" class="pull-right" name="' + obj[i]["NAME"] + '" ><i class="fa fa-ban"></i></a>') +
              '</h4>' +
              '</div>' +
              '</li>'
          );
        }
      }
    }
  });
  $.ajax({
    url: 'http://' + domain + '/eChat/teams/getPublicTeams',
    data: {'created_by': currentUser},
    method: "post",
    async: false,
    success: function (obj) {
      //var obj = jQuery.parseJSON(data);
      if (obj.length > 0) {
        $('.publicTeamList').html('');

        for (var i = 0; i < obj.length; i++) {
          $('.publicTeamList').append(
              '<li class="public_team_List draggable" data-teamID="' + obj[i]["ID"] + '">' +
              '<i class="fa fa-users"></i>' +
              '<div class="publicTeamName">' +
              '<h4>' +
              '<a href="#" class="startChat" data-teamName="' + obj[i]["NAME"] + '" data-channel="private-team-' + obj[i]["ID"] + '_' + site_id + '">' + obj[i]["NAME"] + '</a>' +
              ((obj[i]["CREATED_BY"] == currentUser) ? '<a rel="tooltip" title="Edit this team" data-placement="left" data-container="body" href="#" class="noAjax team_add pull-right" name="Edit Team" data-TeamID="' + obj[i]["ID"] + '"><i class="fa fa-pencil-square"></i></a>' : '') +
              '<a rel="tooltip" data-original-title="Join Team" class="joinTeam margin-right-5 pull-right team_' + obj[i]["ID"] + '" data-teamID="' + obj[i]["ID"] + '" href="##" data-placement="left" style="display:inherit" onclick="joinTeamJS(' + obj[i]["ID"] + ')"><i class="fa fa-user-plus"></i></a>' +
              '</h4>' +
              '</div>' +
              '</li>'
          );
        }
      }
    }
  });
}
loadTeamDataJS();
$(document).off("click", ".inviteUser").on("click", ".inviteUser", function () {
  $(this).closest("li").next().toggle();
});

$(document).off("click", ".team_add").on("click", ".team_add", function (e) {
	var width = document.body.clientWidth;
	console.log(width);
	if(width < 570){
		$('#chatTeams').hide();
		$('#chatUserList').hide();
		$('.right-sidebar').show();
		$('.right-sidebar').css('background','#FFF');
	}

  var team_id = $(this).attr('data-TeamID');
  $('#content-team').empty();
  $('.chatWindow').hide();

  $.ajax({
    url: 'http://' + domain + '/eChat/teams/editTeam/id/' + team_id,
    datatype: "html",
    success: function (data) {
      var data = data.replace(new RegExp("/eunify/avatar?", "g"), 'http://' + domain + '/eunify/avatar?');
      console.log($(data).find('#editTeam'));
      if (team_id > 0) {
        $('#content-team').append('<div class="widget-header"><h4><i class="fa fa-info-circle"></i>Edit Team</h4></div>');
      }
      else {
        $('#content-team').append('<div class="widget-header"><h4><i class="fa fa-info-circle"></i>Add Team</h4></div>');
      }
      $('#content-team').append($(data).find('#editTeam'));
      $('#content-team').append('<button class="btn btn-info addTeam_User">Submit</button>');
      $('#content-team').append('<button class="btn btn-info cancelTeam">Cancel</button>');
      $('#content-team').show();

    }
  });


});
$(document).on('click', '.clear', function(){
	$('.uList').show();
	$('.fList').hide();
	$('.fList').closest('.slimScrollDiv').hide();
	$('#searchUser').val('');
});
$(document).on('keyup', '#searchUser', function () {

	var e = this;
	$('.fList').empty();
	if($(e).val().length){
		$.ajax({
			url: 'http://' + domain + '/eChat/teams/getContacts',
			data: {
			  'search': $(e).val(), 'user_ids': ''
        	},
			async:false,
        	method: "post",
        	success: function (obj) {
			  	$('.uList').hide();
			  	$('.fList').show();
				$(document).find('.slimScrollDiv').show();
			  	//var obj = jQuery.parseJSON(data);
			  	var list = $('#offUsers').val();
			  	var arr = list.split(',');
			  	var chatLink = '';
          		$.each(obj, function (index,DATA) {
			  		if (DATA.CONTACTID == $("#userID").val()) {
						  statusLink = '<span rel="tooltip" data-original-title="User is available" data-placement="left" data-userID = "' + DATA.CONTACTID + '" class="pull-right changeStatus" data-status="online"><i class="fa fa-check-circle txt-color-greenLight"></i></span>';
						}
					else if(jQuery.inArray( ''+DATA.CONTACTID+'', arr) >= 0) {
						  statusLink = '<span rel="tooltip" data-original-title="User is away" data-placement="left" data-userID = "' + DATA.CONTACTID + '" class="pull-right changeStatus" data-status="away"><i class="fa fa-clock-o txt-color-orange"></i></span>';
					  }
					else{
						  statusLink = '<span rel="tooltip" data-original-title="User is unavailable" data-placement="left" data-userID = "' + DATA.CONTACTID + '" class="pull-right changeStatus" data-status="dnd"><i class="fa fa-times-circle txt-color-red"></i></span>';
						}
					if (DATA.CONTACTID != $("#userID").val()) {
							chatLink = '<a rel="tooltip" data-original-title="Start Chat" data-placement="left" href="#" data-userEmail="' + DATA.EMAIL + '" data-userID = "' + DATA.CONTACTID + '" class="pull-right user_' + DATA.CONTACTID + ' startChat" style="display:"inherit" data-userName="' + DATA.FIRST_NAME + ' ' + DATA.SURNAME + '"><i class="fa fa-comments-o"></i></a>'
						}

					$('.fList').append(
						'<li class="userList draggable" data-userID="' + DATA.CONTACTID + '">' +
						'<img src="http://' + domain + '/eunify/avatar?contactID=' + DATA.CONTACTID + '&size=50" height="50" width="50" class="img-thumbnail img-circle">' +
						'<div class="uName">' +
						'<h4>' +
						'<a href="#">' + DATA.FIRST_NAME + ' ' + DATA.SURNAME + '</a>' +
						statusLink +
						chatLink +
						'</h4>' +
						'<p>' + DATA.KNOWN_AS + '</p>' +
						'</div>' +
						'</li>'
					  );
					  $('.fList').slimScroll({
							height: '500px',
							color: '#fff',
							alwaysVisible: true
						});
             		});
				}
  			});
		}
		else{
			$('.uList').show();
			//$('.fList').hide();
			$('.fList').closest('.slimScrollDiv').hide();
		}
	});
$(document).on('click', '.inviteUserSearch', function () {
  var e = this;
  $(e).autocomplete({
    source: function (request, response) {
      $.ajax({
        url: 'http://' + domain + '/eChat/teams/getContacts',
        data: {
          'search': $(e).val(), 'user_ids': $('#teamDatausers').val()
        },
        method: "post",
        success: function (obj) {
        // var obj = jQuery.parseJSON(data);
          response($.map(obj, function (DATA) {
            return {
              label: DATA['FIRST_NAME'] + ' ' + DATA['SURNAME'] + ' "' + DATA['EMAIL'] + '"',
              value: DATA['FIRST_NAME'] + ' ' + DATA['SURNAME'] + ' "' + DATA['EMAIL'] + '"',
              id: DATA['CONTACTID'],
              email: DATA['EMAIL'],
              name: DATA['FIRST_NAME'] + ' ' + DATA['SURNAME'],
              companyname: DATA['KNOWN_AS']
            };
          }));
        }
      });
    },
    minLength: 1,
    select: function (event, ui) {
      $('.users-list-team').prepend(
          '<li  id="userdiv' + ui.item.id + '">' +
          '<div class="inner-user-box">' +
          '<a href="##" class="close-bttn" onclick="removeUserFromList(' + ui.item.id + ')">X</a>' +
          '<img src="http://' + domain + '/eunify/avatar?contactID=' + ui.item.id + '&size=50">' +
          '<div class="user-name">' +
          '<h4>' + ui.item.name + '</h4>' +
          '<span>' + ui.item.companyname + '</span>' +
          '</div>' +
          '</div>' +
          '</li>'
      );
      var teamUsers = $('#teamDatausers').val();
      if (teamUsers.length) {
        $('#teamDatausers').val(teamUsers + ',' + ui.item.id);
      } else {
        $('#teamDatausers').val(ui.item.id);
      }
      $('#add_user').val('');

    },
    open: function () {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function () {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
})

$(document).on('click', '.addTeam_User', function () {
  var teamName = $('#name').val();
  var teamID = $('#teamID').val();
  var teamStatus = $('#team_status').val();
  var teamDatausers = $('#teamDatausers').val();
  $.ajax({
    url: 'http://' + domain + '/eChat/teams/doEditTeam',
    data: {
      'teamData.id': teamID, 'teamData.name': teamName, 'teamData.users': teamDatausers, 'teamData.status': teamStatus
    },
    method: "post",
    success: function (obj) {
      //alert(data);
      //var obj = jQuery.parseJSON(data).DATA;
      $(".teamList").find("[data-teamID='" + obj.DATA.id[0] + "']").remove();
      $('.teamList').append(
          '<li class="team_List draggable" data-teamID="' + obj.DATA.id[0] + '">' +
          '<i class="fa fa-users"></i>' +
          '<div class="teamName">' +
          '<h4>' +
          '<a href="#" name="Edit Team" class="noAjax team_add">' + obj.DATA.name[0] + '</a>' +
          '<a href="#" class="noAjax team_add pull-right" name="Edit Team"><i class="fa fa-pencil-square"></i></a>' +
          '</h4>' +
          '</div>' +
          '</li>'
      );
      $('#content-team').empty();
    }
  });

});
$(document).on('click', '.cancelTeam', function () {
  $('#content-team').empty();
});

if ($(".chatWindow").is(":visible") == true) {
  //alert ("here");
}


function removeValueFromList(list, value, separator) {
  separator = separator || ",";
  var values = list.split(separator);
  for (var i = 0; i < values.length; i++) {
    if (values[i] == value) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }
  return list;
}
function removeUserFromList(id) {
  $("#userdiv" + id).remove();
  var teamUsers = $('#teamDatausers').val();
  $('#teamDatausers').val(removeValueFromList(teamUsers, id, ','));
}
function joinTeamJS(id) {
  $.ajax({
    url: 'http://' + domain + '/eChat/teams/saveUserInPublicTeam',
    data: {'teamid': id},
    method: "post",
    async: false,
    success: function (data) {
      $("li[data-teamid=" + id + "]").remove();
      loadTeamDataJS();

    }
  });
}
function deleteTeam(elem) {
  var TeamID = $(elem).attr("id");
  var TeamName = $(elem).attr("name");
  $.get("http://" + domain + "/eChat/teams/deleteTeam?id=" + TeamID + "&fwCache=true", function (data) {
    $("li[data-teamid=" + TeamID + "]").remove();
  });

}
function leaveGroup(elem) {
  var TeamID = $(elem).attr("id");
  var TeamName = $(elem).attr("name");
  $.get("http://" + domain + "/eChat/teams/leaveTeam?id=" + TeamID + "&fwCache=true", function (data) {
    $("li[data-teamid=" + TeamID + "]").remove();
  });

}
function seenMessage(obj) {
  var chatHash = $(obj).closest('.tab-pane').attr('id');
  $.ajax({
    url: "http://" + domain + "/eChat/chat/seenChat",
    data: {
      chathash: chatHash
    },
    method: "POST",
    success: function () {
      // $(thisE).val("").focus();
    }
  });
}

function resizeWindow() {
  var cHeight = (($(window).innerHeight() - 245));
  $("#chatWindows .chatHistory").css({"height": cHeight + "px"});
}

function showNotification(requestedChatData, chatData, buildChat) {
	
$.ajax({
  url: "http://" + domain + "/eChat/teams/notyToDevice",
  data: {'regIds': $('#dev_reg_ID').val(), 'chatMessage': chatData.message, 'chatUser':chatData.userName},
  async: false,
  success: function (data) {
	  var hasPermission = true;
	  console.log(hasPermission);
	  var chatMessage = chatData.message;
	  var chatUserID = chatData.userID;
	  var chatTime = chatData.timestamp;
	  var userName = $("#userName").val();
	  var userEmail = $("#userEmail").val();
	  var userID = $("#userID").val();
	  var existingChat = localStorage.getObject("chatWindows");
	  var chatUsers = chatData.chatUsers;
	  console.log(chatData);
	  console.log(requestedChatData);
	  var thisChat = chatData.chatHash;
	  var chatExists = existingChat.hasOwnProperty(thisChat);
	  if (!chatExists) {
		existingChat[thisChat] = requestedChatData;
		localStorage.setObject("chatWindows", existingChat);
		console.log("Set local storage");
		console.log(localStorage.getObject("chatWindows"));
	  }
	
	  if (!$(".chatWindow").is("visible")) {
		$(".chatWindow").show();
	  }
	  //
	  if (!chatExists) {
		if (existingChat[thisChat].hasOwnProperty("teamName") && existingChat[thisChat]["teamName"] != "") {
		  var chatTitle = existingChat[thisChat]["teamName"];
		} else {
		  var chatTitle = requestedChatData.userNames;
		  chatTitle.splice($.inArray($("#userName").val(), requestedChatData.userNames), 1);
		}
		var thisChatPane = $(
			'<div class="droppable tab-pane fade in" id="' + thisChat + '">' +
			'<div class="chatHistory">' +
			createChatMore(thisChat) +
			'</div>' +
			'</div>' +
			createChatTextBox(userID, userName, thisChat) +
			'</div>'
		);
		$.ajax({
		  url: "http://" + domain + "/eChat/archive/history?isAjax=true&chatHash=" + thisChat,
		  success: function (data) {
			var data = data.replace(new RegExp("/eunify/avatar?", "g"), 'http://' + domain + '/eunify/avatar?');
			var width = document.body.clientWidth;
				console.log(width);
				if(width < 570){
					$('#chatTeams').hide();
					$('#chatUserList').hide();
					$('.right-sidebar').css('background','#fff');
					$('.right-sidebar').show();
				}
			$("#chatWindows").append(
				'<div class="droppable tab-pane fade in" id="' + thisChat + '">' +
				createChatMore(thisChat) +
				'<div class="chatHistory"></div>' +
				createChatTextBox(userID, userName, thisChat) +
				'</div>'
			);
			$("#chatWindows #" + thisChat + " .chatHistory").append($(data).find('.chatItem'));
			resizeWindow();
			//$("#chatWindows .chatHistory").last().scrollbox("update");
			$("#chatTabs").append(
				'<li class="realTab">' +
				'<a data-chatID="' + thisChat + '" href="#" data-target="#' + thisChat + '" data-toggle="tab"><span>' + chatTitle + '</span> <i data-container="body" data-placement="right" data-original-title="Close Chat Tab" rel="tooltip" class="fa fa-times-circle closeChatTab"></i></a>' +
				'</li>'
			);
	
			$("#chatTabs a:last").tab("show");
			$("#" + thisChat + " .chatHistory").scrollTop($("#" + thisChat + " .chatHistory")[0].scrollHeight);
		  }
		});
	  }
	  if (hasPermission && currentStatus == "away") {
		var myNotification = new Notification("Message from " + chatData.userName, {
		  body: chatData.message
		});
		if (!remote.getGlobal('mainWindow').isVisible()) {
		  remote.getGlobal('mainWindow').showInactive();
		  remote.getGlobal('mainWindow').minimize();
		  remote.getGlobal('mainWindow').flashFrame(true);
		}
	
	
		myNotification.onclick = function () {
		  if (remote.getGlobal('mainWindow').isMinimized()) {
			remote.getGlobal('mainWindow').restore();
			remote.getGlobal('mainWindow').flashFrame(false);
		  }
	
		  window.focus();
		}
	  }
  }
});
	
 
}
$(function(){
	$('.teamList').slimScroll({
		height: '200px',
		color: '#fff',
		alwaysVisible: true,
		railVisible: true
	});
	
	$('.publicTeamList').slimScroll({
		height: '200px',
		color: '#fff',
		railVisible: true,
		alwaysVisible: true
	});
});